using PyPlot

# Constants
a = 1.5
b = 0.0002
c = 0.0000
d = 0.0001
e1 = 0.05
e2 = 0.16

figureNum = 0
figureTitle = ["u1 = 0, u2 > 0" "u1 > 0, u2 > 0" "u1 > 0, u2 = 0"]

function f(t, x)
    # return this value
    [a*x[1] - b*(x[1])^2 - e1*x[1]*x[2], c*x[2] - d*(x[2])^2 + e2*x[1]*x[2]]
end

function runge_kutta(x0,t0,alpha,n,showGraphs, const_tuple)
    global a, b, c, d, e1, e2
    a = const_tuple[1]; b = const_tuple[2]; c = const_tuple[3]; d = const_tuple[4]; e1 = const_tuple[5]; e2 = const_tuple[6];
    t = [1.0:n;]
    x = [1.0 for r in 1:2, c in 1:n]
    dt = alpha / n
    tj = t0
    xj = x0
    for j = 0:n-1
        l1 = f(tj, xj)
        l2 = f(tj+0.5*dt, xj+0.5*dt*l1)
        l3 = f(tj+0.5*dt, xj+0.5*dt*l2)
        l4 = f(tj+dt, xj+dt*l3)
        t[j+1] = tj + dt
        x[:,j+1] = xj + dt/6*(l1 + 2*(l2 + l3) + l4);
        tj = t[j+1]
        xj = x[:,j+1]
    end

    global figureNum
    if showGraphs == true
        figureNum = figureNum + 1
        fig = PyPlot.figure(figureNum, figsize=(15,5))
        PyPlot.subplot(1,3,1)
        fig1 = PyPlot.plot(vec(x[1,:]), vec(x[2,:]))
        PyPlot.title(figureTitle[figureNum])
        PyPlot.xlabel("x1"); PyPlot.ylabel("x2");
        PyPlot.subplot(1,3,2)
        fig2 = PyPlot.plot(t[:], vec(x[1,:]))
        PyPlot.title(figureTitle[figureNum])
        PyPlot.xlabel("t"); PyPlot.ylabel("x1");
        PyPlot.subplot(1,3,3)
        fig3 = PyPlot.plot(t[:], vec(x[2,:]))
        PyPlot.title(figureTitle[figureNum])
        PyPlot.xlabel("t"); PyPlot.ylabel("x2");
    end

    touchdown_x1 = false
    touchdown_x2 = false
    # Check touchdown
    for j = 1:n
        if x[1,j] < 1e-3
            touchdown_x1 = true
        elseif x[2,j] < 1e-3
            touchdown_x2 = true
        end
    end
    (touchdown_x1, touchdown_x2, x[1,n],x[2,n]) # return this tuple
end

println(runge_kutta([20.0;5.0], 0, 1.0, 100000, true, (20.0, 1.0, 30.0, 1.0, 2.0, 2.0)))
println(runge_kutta([200.0;100.0], 0, 10.0, 100000, true, (10, 0.0002, 0.0001, 0.01, 0.05, 0.03)))
println(runge_kutta([30, 10], 0, 300.0, 300000, true, (20.0, 1.0, -20.0, 0.0, 4.0, 1.0)))